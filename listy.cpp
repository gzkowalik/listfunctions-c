#include <iostream>
using namespace std;
struct node2
{
	int val, x, y;
	node2* next;
};
struct node
{
	int val;
	node* next;
};
void pushfront(node* &l1, int val)
{
	node* p = new node;
	p->val = val;
	p->next = l1;
	l1 = p;

}
void deletein(node* &first)
{
	node* p = first;
	int max = 0;
	int licz = 0;
	int maxpocz = 1;
	int maxkonc = 0;
	int pocz = 1;
	int biez = 0;
	bool czydwa = false;
	int prev = INT16_MIN;
	if (p == NULL)
	{
		cout << "Brak lancucha " << endl;
	}
	while (p != NULL)
	{
		licz++;
		if (p->val > prev)
			biez++;
		else
		{
			if (biez > max)
			{
				maxkonc = licz;
				maxpocz = pocz;
				pocz = licz;
				max = biez;
				biez = 0;
				czydwa = false;
			}
			else if (biez == max)
			{
				czydwa = true;
				pocz = licz;
				biez = 0;
			}
			else
			{
				biez = 0;
				pocz = licz;
			}

		}
		prev = p->val;
		p = p->next;

	}
	if (!czydwa)
	{
		node* temp = first;
		node* previ = first;
		for (int i = 1; i < maxpocz; i++)
		{
			previ = temp;
			temp = temp->next;
		}
		int l_us = 0;
		while (l_us != maxkonc - maxpocz + 1)
		{
			node* przechowaj = temp;
			temp = temp->next;
			previ->next = temp;
			delete przechowaj;
			l_us++;
		}
	}
}
node2* convert(int t[2][2])
{
	node2* Node = new node2;
	Node = NULL;

	for (int i = 0; i < 2; i++)
	{
		
		for (int j = 0; j < 2; j++)
		{
			if (t[i][j] != 0)
			{
				
				node2* temp = new node2;
				temp->x = i;
				temp->y = j;
				temp->val = t[i][j];
				temp->next = Node;
				Node = temp;
			}
		}
	}
	return Node;

}
void pushback(node* &first, int val)
{

	node* n = first;
	if (n == NULL)
	{
		pushfront(n, val);
		first = n;
	}
	else
	{
		while (n->next != NULL)
			n = n->next;
		node* p = new node;
		p->val = val;
		n->next = p;
		p->next = NULL;
	}

}
void display(node2* Node)
{
	while (Node!= NULL)
	{
		cout << Node->val << " (" << Node->x << "," << Node->y << ") ->";
		Node = Node->next;
	}
	cout << "NULL" << endl;
}
void display(node* Node)
{
	while (Node != NULL)
	{
		cout << Node->val << "->";
		Node = Node->next;
	}
	cout << "NULL" << endl;
}
node* polacz(node* l1, node* l2)
{
	node* l = new node;
	l = NULL;
	while ((l1 != NULL) || (l2 != NULL))
	{
		if (l1 == NULL)
		{
			while (l2 != NULL)
			{
				pushfront(l, l2->val);
				l2 = l2->next;
			}
		}
		else if (l2 == NULL)
		{
			while (l1 != NULL)
			{
				pushfront(l, l1->val);
				l1 = l1->next;
			}
		}
		else
		{
			if (l1->val < l2->val)
			{
				pushback(l, l1->val);
				l1 = l1->next;
			}
			else if (l1->val == l2->val)
			{
				pushback(l, l1->val);
				l1 = l1->next;
				l2 = l2->next;
			}
			else
			{
				pushback(l, l2->val);
				l2 = l2->next;
			}

		}
	}
	
	return l;
}
int skasuj(node* &l1, node* &l2)
{
	int licznik = 0;
	if ((l1 == NULL) || (l2 == NULL))
		return licznik;
	node* p = l1;
	node* q = l2;
	while (q != NULL)
	{
		
//		cout << "petla q = " << q->val << endl;
	//	cout << "petla p = " << p->val << endl;
		while (p != NULL)
		{	
			if (q->val == p->val)
			{
				cout << "Usuwamy: " << p->val << " i " << q->val << endl;
				node* t1 = l1;
				node* t2 = l2;
				if (t1 != p)
				{
					while (t1->next != p)
						t1 = t1->next;
					node* temp = p;
					p = p->next;
					t1->next = p;
					delete temp;
				}
				else
				{
					node* temp = l1;
					l1 = l1->next;
					delete temp;
					
				}

				if (t2 != q)
				{
					while (t2->next != q)
						t2 = t2->next;
					node* temp2 = q;
					q = q->next;
					t2->next = q;
					delete temp2;
				}
				else
				{
					node* temp2 = l2;
					l2 = l2->next;
					q = q->next;
					delete temp2;
					
				}	
				licznik++;
				break;
			}

			else if (q->val > p->val)
				p = p->next;
		}
		p = l1->next;
		q = q->next;
		
	}
	return licznik;
}
void podziel(node* &l, node* &l1, node* &l2)
{
	if (l != NULL)
	{
		node* p = l;
		while (p != NULL)
		{
			if (p->val % 3 == 1)
			{
				pushfront(l1, p->val);
				p = p->next;
				
			}
				
			else if (p->val % 3 == 2)
			{
				pushfront(l2, p->val);
				p = p->next;
			}
				
			else
			{
				node *temp = l;
				while (temp->next != p)
					temp = temp->next;
				//cout << "Do usuniecia: " << temp->next->val << endl;
				node *to = p;
				p = p->next;
				temp->next = p;
				
				delete to;
			}
			//cout << "wychodze" << endl;

		}

	}
}
int ilebitowa(int val)
{
	int liczbaJedynek = 0;
	while (val > 0)
	{
		if (val % 2 == 1)
			liczbaJedynek++;
		val /= 2;
	}
	if (liczbaJedynek < 8)
		return 0;
	else if ((liczbaJedynek >= 8) && (liczbaJedynek <= 24))
		return 1;
	else
		return 2;
}
void deleteList(node* &first)
{
	while (first != NULL)
	{
		node* temp = first;
		first = first->next;
		delete temp;
	}
	delete first;
}
node* podzielnabity(node* l)
{
	node* tab[3];
	for (int i = 0; i < 3; i++)
	{
		tab[i] = NULL;
	}
	while (l != NULL)
	{
		int ktora = ilebitowa(l->val);
		pushfront(tab[ktora], l->val);
		l = l->next;
	}
	deleteList(l);
	for (int i = 0; i < 3; i++)
	{
		display(tab[i]);
	}
	node* k = tab[0];
	while (tab[0]->next != NULL)
	{
		tab[0] = tab[0]->next;
	}
	tab[0]->next = tab[1];
	while (tab[1]->next != NULL)
	{
		tab[1] = tab[1]->next;
	}
	tab[1]->next = tab[2];
	return k;
		
}
int search(node* first, int val)
{
	int whereIs = 1;
	while (first != NULL)
	{
		if (first->val == val)
		{
			return whereIs;
		}
		else
		{
			first = first->next;
			whereIs++;
		}
	}
	return 0;
}
void deleteOrAdd(node* &first, int val)
{
	if (!search(first, val))
		pushback(first, val);
	else
	{
		if (search(first, val) == 1)
		{
			node* temp = first;
			first = first->next;
			delete temp;
		}
		else
		{
			node* p = first;
			for (int i = 1; i < search(first, val); i++)
				p = p->next;
			node* temp = p->next;
			p->next = temp->next;
			delete temp;
		}
		
	}
}
node* create(int val, node* first, int i)
{
	if (i <= val)
	{
		pushback(first, i);
		create(val, first, i + 1);
	}
	return first;
}
node* stworz(node* &l1, node* &l2)
{
	node* nowa = l1;
	node* p = l2;
	while (p != NULL)
	{
		int a = search(nowa, p->val);
		if (a == 0)
		{
			pushfront(nowa, p->val);
		}
		p = p->next;
	}
	while (nowa != NULL)
	{
		cout << nowa->val << "->";
		nowa = nowa->next;
	}
	cout << "NULL" << endl;
	deleteList(l1);
	deleteList(l2);
	
	return nowa;
}
const int N = 5;
bool cyfry(int a, int b)
{
	int tab1[10] = { 0 };
	int tab2[10] = { 0 };
	int cyfra;
	while (a > 0)
	{
		cyfra = a % 10;
		tab1[cyfra]++;
		a /= 10;
	}
	while (b > 0)
	{
		cyfra = b % 10;
		tab2[cyfra]++;
		b /= 10;
	}

	for (int i = 0; i < 10; i++)
	{
		if ((tab1[i] != 0) && (tab2[i] != 0))
			return true;
	}
	return false;
}
int ilewezlow(node* first)
{
	int licznik = 0;
	node* p = first;
	while (p != NULL)
	{
		licznik++;
		p = p->next;
	}
	return licznik;
}
node* y(node* &l1, node *&l2)
{
	int lb1 = ilewezlow(l1);
	int lb2 = ilewezlow(l2);
	if (lb1 > lb2)
	{
		int r = lb1 - lb2;
		for (int i = 1; i <= r; i++)
		{
			node* temp = l1;
			l1 = l1->next;
			delete temp;
		}
	}
		while ((l1 != l2) && (l1!=NULL) && (l2!=NULL))
		{
			node* temp1 = l1;
			l1 = l1->next;
			delete temp1;
			node* temp2 = l2;
			l2 = l2->next;
			delete temp2;
		}
	return l1;

}
void f(int a, int b)
{
	cout << "a: " << a << "b: " << b << endl;
}

int main()
{
	
	return 0;
}